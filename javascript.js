window.onload = function() {
	Init.init();
}

var Utils = new function UtilsClass() {
	var self = this;

	self.modifyAttribute = function(id, attribute, newValue) {
		var e = document.getElementById(id);
		e.setAttribute(attribute, newValue);
	}

	self.modifyInnerHTML = function(id, newValue) {
		var e = document.getElementById(id);
		e.innerHTML = newValue;
	}
}

var Init = new function InitClass() {
	var self = this;

	self.init = function() {
		populateBody();
		// Utils.modifyInnerHTML("CPSLabel", "CPS: " + Global.curCPS);
	}

	var populateBody = function() {
		var tableBody = document.getElementById("tableBody");
		for (var upgrade in Global.upgrades) {
			var curU = Global.upgrades[upgrade];

			var upgradeTR = document.createElement("tr");
			upgradeTR.setAttribute("controlsUpgrade", upgrade);

			var name = document.createElement("td");
			name.innerHTML = upgrade;
			upgradeTR.appendChild(name);

			var levelCell = document.createElement("td");
			var levelTextBox = document.createElement("input");
			levelTextBox.setAttribute("type", "text");
			levelTextBox.setAttribute("id", upgrade + "TextBox");
			levelTextBox.setAttribute("size", 3);
			levelTextBox.value = Global.upgrades[upgrade].level;
			levelCell.appendChild(levelTextBox);

			var setButton = document.createElement("input");
			setButton.setAttribute("type", "button");
			setButton.setAttribute("onclick", "GUI.setButtonClicked(\"" + upgrade + "\")");
			setButton.value = "Set";
			levelCell.appendChild(setButton);

			var upgradeButton = document.createElement("input");
			upgradeButton.setAttribute("type", "button");
			upgradeButton.setAttribute("onclick", "GUI.upgradeButtonClicked(\"" + upgrade + "\")");
			upgradeButton.value = "Upgrade";
			levelCell.appendChild(upgradeButton);

			var downgradeButton = document.createElement("input");
			downgradeButton.setAttribute("type", "button");
			downgradeButton.setAttribute("onclick", "GUI.downgradeButtonClicked(\"" + upgrade + "\")");
			downgradeButton.value = "Downgrade";
			levelCell.appendChild(downgradeButton);

			upgradeTR.appendChild(levelCell);

			curU.cost = Math.floor(curU.bPrice * (Math.pow(1.3, curU.level)));
			var costCell = document.createElement("td");
			costCell.setAttribute("id", upgrade + "CostCell");
			costCell.innerHTML = curU.cost.toLocaleString();
			upgradeTR.appendChild(costCell);

			var totalSpentCell = document.createElement("td");
			totalSpentCell.setAttribute("id", upgrade + "TotalSpentCell");
			totalSpentCell.innerHTML = Math.round(curU.bPrice * ((Math.pow(1.3, curU.level) - 1) / 0.3)).toLocaleString();
			upgradeTR.appendChild(totalSpentCell);

			var cpsCell = document.createElement("td");
			cpsCell.setAttribute("id", upgrade + "CPSCell");
			cpsCell.innerHTML = curU.cps.toLocaleString();
			upgradeTR.appendChild(cpsCell);

			var curCPScell = document.createElement("td");
			curCPScell.setAttribute("id", upgrade + "CurCPSCell");
			curCPScell.innerHTML = (curU.level * curU.cps).toLocaleString() + "(" + Math.round(curU.level * curU.cps * 10000 / Global.curCPS) / 100 + "%)";
			upgradeTR.appendChild(curCPScell);

			curU.cpc = (Math.floor(curU.cost * (curU.cps + Global.curCPS) / curU.cps + 0.5)).toLocaleString();
			var cpc = document.createElement("td");
			cpc.setAttribute("id", upgrade + "CPCCell");
			cpc.innerHTML = curU.cpc.toLocaleString();
			upgradeTR.appendChild(cpc);

			tableBody.appendChild(upgradeTR);

			GUI.updateInfo(curU.level, upgrade);

		}
		document.getElementById("mass_update_button").setAttribute("onclick", "GUI.massUpdateButtonClicked()");
		GUI.highLightLowest();
	}
}

// 6,833,952

var GUI = new function GUIClass() {
	var self = this;
	var buttonsDisabled = false;

	self.setButtonClicked = function(controlsUpgrade) {
		var levelVal = parseInt(document.getElementById(controlsUpgrade + "TextBox").value);
		localStorage.setItem(controlsUpgrade, levelVal);
		self.updateInfo(levelVal, controlsUpgrade);
	}

	self.upgradeButtonClicked = function(controlsUpgrade) {
		var textbox = document.getElementById(controlsUpgrade + "TextBox");
		var levelVal = parseInt(textbox.value) + 1;
		textbox.value = levelVal;
		self.setButtonClicked(controlsUpgrade);
	}

	self.downgradeButtonClicked = function(controlsUpgrade) {
		var textbox = document.getElementById(controlsUpgrade + "TextBox");
		var levelVal = parseInt(textbox.value) - 1;
		textbox.value = levelVal;
		self.setButtonClicked(controlsUpgrade);
	}

	self.massUpdateButtonClicked = function() {
		if (buttonsDisabled) {
			$('#tableBody').find("input[type=button][value='Set']").trigger('click');
		}
		$('#tableBody').find("input[type=button]").attr('disabled', !buttonsDisabled)
		buttonsDisabled = !buttonsDisabled;
	}

	self.updateInfo = function(levelVal, controlsUpgrade) {

		var curU = Global.upgrades[controlsUpgrade];
		curU.level = levelVal;
		if (controlsUpgrade == "PowerClick") {
			curU.cps = Math.pow(2, curU.level) * Global.tapsPerSecond;
			var cps = document.getElementById(controlsUpgrade + "CPSCell");
			cps.innerHTML = curU.cps;
		}

		curU.cost = Math.floor(curU.bPrice * (Math.pow(curU.increaseRate || 1.3, curU.level)));
		var costCell = document.getElementById(controlsUpgrade + "CostCell");
		costCell.innerHTML = curU.cost.toLocaleString();

		var totalSpentCell = document.getElementById(controlsUpgrade + "TotalSpentCell");
		totalSpentCell.innerHTML = Math.round(curU.bPrice * ((Math.pow(1.3, curU.level) - 1) / 0.3)).toLocaleString();

		// var curCPScell = document.getElementById(controlsUpgrade + "CurCPSCell");
		// curCPScell.innerHTML = (curU.level * curU.cps).toLocaleString() + "(" + Math.round(curU.level * curU.cps * 10000 / Global.curCPS) / 100 + "%)";

		// var cpc = document.getElementById(controlsUpgrade + "CPCCell");
		// cpc.innerHTML = Math.floor(curU.bPrice * (Math.pow(1.3, curU.level)) / curU.cps + 0.5).toLocaleString();


		self.highLightLowest();
		Utils.modifyInnerHTML("CPSLabel", "CPS: " + Global.curCPS.toLocaleString());
		Utils.modifyInnerHTML("PowerCookieValue", "Power Cookie Value: " + (Global.curCPS * 240).toLocaleString());
	}

	self.highLightLowest = function() {
		var array = [];
		var curCPS = 0;
		for (var u in Global.upgrades) {
			array.push(Global.upgrades[u]);
			curCPS += Global.upgrades[u].level * Global.upgrades[u].cps;
		}
		Global.curCPS = curCPS;
		console.log(curCPS);
		array.sort(function(A, B) {
			return A.cost * (A.cps + curCPS) / A.cps - B.cost * (B.cps + curCPS) / B.cps;
		});

		// console.log(array);

		for (var u in Global.upgrades) {
			var A = Global.upgrades[u];
			var costCell = document.getElementById(u + "CostCell");
			if (!costCell) continue;
			costCell.setAttribute("style", "background-color:none;");

			var CPCcell = document.getElementById(u + "CPCCell");
			CPCcell.innerHTML = (Math.floor(A.cost * (A.cps + curCPS) / A.cps + 0.5)).toLocaleString();

			var curCPScell = document.getElementById(u + "CurCPSCell");
			curCPScell.innerHTML = (A.level * A.cps).toLocaleString() + "(" + Math.round(A.level * A.cps * 10000 / Global.curCPS) / 100 + "%)";
		}
		var costCell = document.getElementById(array[0].name + "CostCell");
		costCell.setAttribute("style", "background-color:yellow;");
	}
}

var Global = new function GlobalClass() {
	var self = this;

	self.increaseRate = 1.3;
	self.tapsPerSecond = 6.1;

	self.upgrades = {
		// "PowerClick": {
		// 	bPrice: 5000,
		// 	increaseRate: 2.5,
		// 	cps: self.tapsPerSecond
		// },
		"AutoClick": {
			bPrice: 30,
			cps: 0.1
		},
		"GrandMa": {
			bPrice: 100,
			cps: 0.3
		},
		"C-Robot": {
			bPrice: 1000,
			cps: 1
		},
		"CookieFarm": {
			bPrice: 10000,
			cps: 3.1
		},
		"C-Factory": {
			bPrice: 50000,
			cps: 6
		},
		"S-Factory": {
			bPrice: 200000,
			cps: 20
		},
		"X-Factory": {
			bPrice: 500000,
			cps: 40
		},
		"C-Cloner": {
			bPrice: 1000000,
			cps: 70
		},
		"C-Cern": {
			bPrice: 5000000,
			cps: 300
		},
		"Atomic-C": {
			bPrice: 30000000,
			cps: 1500
		},
		"Alien Robot": {
			bPrice: 70000000,
			cps: 3000
		},
		"Alien Labs": {
			bPrice: 200000000,
			cps: 8000
		},
		"Alien Lab v2": {
			bPrice: 400000000,
			cps: 15000
		},
		"Alien Tech": {
			bPrice: 600000000,
			cps: 20000
		},
		"Alien C-X": {
			bPrice: 1000000000,
			cps: 30000
		},
		"Nano Cookie": {
			bPrice: 2000000000,
			cps: 80000
		},
		"Molecular-C": {
			bPrice: 5000000000,
			cps: 120000
		},
		"Virus Cookie": {
			bPrice: 10000000000,
			cps: 200000
		},
		"Proto Cookie": {
			bPrice: 20000000000,
			cps: 350000
		},
		"Synaptic-C": {
			bPrice: 50000000000,
			cps: 600000
		},
		"Hydrogenic-C": {
			bPrice: 100000000000,
			cps: 1000000
		},
		"Uranium-C": {
			bPrice: 200000000000,
			cps: 1500000
		},
		"Plutonium-C": {
			bPrice: 400000000000,
			cps: 2500000
		},
		"Krypto-C": {
			bPrice: 800000000000,
			cps: 4000000
		},
		"RedKrypto-C": {
			bPrice: 1600000000000,
			cps: 7000000
		},
		"Moon-C": {
			bPrice: 2500000000000,
			cps: 10000000
		},
		"Galaxy-C": {
			bPrice: 4000000000000,
			cps: 15000000
		},
		"Galaxy-X": {
			bPrice: 8000000000000,
			cps: 25000000
		},
		"Cookie Hack": {
			bPrice: 15000000000000,
			cps: 40000000
		},
		"Cookie God": {
			bPrice: 1000000000000000,
			cps: 1000000000
		}
	}

	self.curCPS = 0;

	var init = function() {
		for (var u in self.upgrades) {
			self.upgrades[u].name = u;
			self.upgrades[u].level = parseInt(localStorage.getItem(u) || 0);
			self.curCPS += self.upgrades[u].level * self.upgrades[u].cps;
		}
	}

	init();
}